import { Match } from "./match";
import { MatchStatus } from "./match-status";

export interface MatchData {
    matches: Record<number, Match>
    name: string;
    status: MatchStatus;
}