export interface Match {
    firstTeam: string;
    firstTeamGoals: number;
    secondTeam: string;
    secondTeamGoals: number;
}