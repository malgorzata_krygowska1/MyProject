import { Match } from "./match";

export const Matches: Record<number, Match> = {1: {firstTeam: 'Poland', firstTeamGoals: 0, 
secondTeam: 'Germany', secondTeamGoals: 0}, 2: {firstTeam: 'Brazil', firstTeamGoals: 0, 
secondTeam: 'Mexico', secondTeamGoals: 0}, 3: {firstTeam: 'Argentina', firstTeamGoals: 0, 
secondTeam: 'Uruguay', secondTeamGoals: 0}}