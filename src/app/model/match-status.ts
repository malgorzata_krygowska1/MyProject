export enum MatchStatus {
    NotStarted,
    InProgress,
    Finished
}