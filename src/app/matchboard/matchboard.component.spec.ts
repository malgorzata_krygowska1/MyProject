import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchboardComponent } from './matchboard.component';
import { MatchStatus } from '../model/match-status';

describe('MatchboardComponent', () => {
  let component: MatchboardComponent;
  let fixture: ComponentFixture<MatchboardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MatchboardComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(MatchboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should get total goals, no data', () => {
    component.data = {matches: {}, name: '', status: MatchStatus.NotStarted};
    expect(component.getTotalGoals()).toBe(0);
  });

  it('should get total goals, no goals scored yet', () => {
    component.data = {matches: {1: {firstTeam: 'A', firstTeamGoals: 0, secondTeam: 'B', secondTeamGoals:0}}, name: 'Name', status: MatchStatus.NotStarted}
    expect(component.getTotalGoals()).toBe(0);
  });

  it('should get total goals', () => {
    component.data = {matches: {1: {firstTeam: 'Poland', firstTeamGoals: 1, 
    secondTeam: 'Germany', secondTeamGoals: 2}, 2: {firstTeam: 'Brazil', firstTeamGoals: 3, 
    secondTeam: 'Mexico', secondTeamGoals: 4}, 3: {firstTeam: 'Argentina', firstTeamGoals: 5, 
    secondTeam: 'Uruguay', secondTeamGoals: 6}}, name: 'Name', status: MatchStatus.NotStarted}
    expect(component.getTotalGoals()).toBe(21);
  });
});
