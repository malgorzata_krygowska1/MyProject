import { Component, EventEmitter, Input, Output } from '@angular/core';
import { MatchData } from '../model/mach-data';
import { MatchStatus } from '../model/match-status';
import { Goal } from '../model/goal';

@Component({
  selector: 'app-matchboard',
  templateUrl: './matchboard.component.html',
  styleUrl: './matchboard.component.scss'
})
export class MatchboardComponent {

@Input() data: MatchData = {matches: {}, name: '', status: MatchStatus.NotStarted}
@Input() set goals (goal: Goal | null) {
  this.assignGoals(goal);
}
@Input() status: MatchStatus = MatchStatus.NotStarted;
@Output() startMatch = new EventEmitter();
@Output() restartMatch = new EventEmitter();

matchStatus = MatchStatus;

  getTotalGoals(): number {
    const values = Object.values(this.data.matches);
    const firstTeamGoals = values.map(value => value.firstTeamGoals);
    const secondTeamGoals = values.map(value => value.secondTeamGoals);
    const totalGoals = firstTeamGoals.concat(secondTeamGoals);
    return totalGoals.length ? this.sumTotalGoals(totalGoals) : 0
  }

  private assignGoals(goal: Goal | null) {
    if (goal) {
      if (goal.team === 0) {
        this.data.matches[goal.match].firstTeamGoals = this.data.matches[goal.match].firstTeamGoals + 1;
      } else {
        this.data.matches[goal.match].secondTeamGoals = this.data.matches[goal.match].secondTeamGoals + 1;
      }
    }
  }

  private sumTotalGoals(totalGoals: number[]) {
    return totalGoals.reduce(function(a, b){
      return a + b;
    });
  }
}
