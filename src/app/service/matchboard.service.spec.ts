import { TestBed } from '@angular/core/testing';

import { MatchboardService } from './matchboard.service';

describe('MatchboardService', () => {
  let service: MatchboardService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MatchboardService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
