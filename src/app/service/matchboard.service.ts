import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MatchboardService {

  generateGoals(numberOfElements: number) {
    return Math.floor(Math.random() * numberOfElements);
  }
}