import { Component } from '@angular/core';
import { Matches } from './model/match-constatn';
import { MatchData } from './model/mach-data';
import { MatchStatus } from './model/match-status';
import { MatchboardService } from './service/matchboard.service';
import { Subscription, finalize, take, timer } from 'rxjs';
import { Goal } from './model/goal';
import { EMPTY_SUBSCRIPTION } from 'rxjs/internal/Subscription';
import {cloneDeep} from 'lodash';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  constructor(private readonly matchboardService: MatchboardService){}
  data: MatchData = {matches: cloneDeep(Matches), name: 'Katar 2023', status: MatchStatus.NotStarted};
  status = MatchStatus.NotStarted;
  goals!: Goal;
  goals$: Subscription = EMPTY_SUBSCRIPTION;
  
  startMatch() {
    this.status = MatchStatus.InProgress;
    this.goals$ = timer(10000, 10000).pipe(
      take(9),
      finalize(() => this.status = MatchStatus.Finished)
      ).subscribe(() => this.goals = {match: this.matchboardService.generateGoals(Object.keys(this.data.matches).length) + 1, team: this.matchboardService.generateGoals(2)} as Goal);
  }

  restartMatch() {
    this.goals$.unsubscribe();
    this.data = {... this.data, matches: cloneDeep(Matches)};
    this.startMatch();
  }
}
